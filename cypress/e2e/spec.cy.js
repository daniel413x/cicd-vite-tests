/* eslint-disable no-undef */

describe('Home component interactions', () => {
  beforeEach(() => {
    cy.visit(Cypress.env('CLIENT_URL'));
  });

  it('should successfully make a POST request', () => {
    cy.intercept('POST', `${Cypress.env('API_URL')}/test`).as('postRequest');
    cy.get('button[data-cy="post-btn"]').click();
    cy.wait('@postRequest').its('response.statusCode').should('eq', 200);
    cy.contains('/api/test succeeded: true').should('exist');
  });

  it('should successfully fetch a string via GET request', () => {
    cy.intercept('GET', `${Cypress.env('API_URL')}/string/default`).as('getRequest');
    cy.get('button[data-cy="get-btn"]').click();
    cy.wait('@getRequest').its('response.statusCode').should('eq', 200);
    cy.contains('/api/string/default: string from server').should('exist');
  });
});
